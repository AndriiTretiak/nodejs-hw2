const { User } = require("./models/Users.js");
const bcrypt = require("bcrypt");

const getUser = (req, res, next) => {
  try {
    return User.find({ _id: req.user.userId }).then((result) => {
      res.status(200).json({ user: result });
    });
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

const deleteUser = (req, res, next) => {
  try {
    return User.deleteOne({ _id: req.params.id }).then((result) => {
      res.status(200).send({ message: "success" });
    });
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

const updateUserPassword = async (req, res, next) => {
  const { newPassword } = req.body;
  const password = newPassword;
  try {
    return User.findByIdAndUpdate(req.user.userId, {
      password: await bcrypt.hash(req.body.newPassword, 10),
    }).then((result) => {
      res
        .status(200)
        .send({ message: "success", newPass: req.body.newPassword });
    });
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

module.exports = {
  getUser,
  deleteUser,
  updateUserPassword,
};
