const { User } = require("./models/Users.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const registerUser = async (req, res, next) => {
  const { username, password } = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  user
    .save()
    .then(() => res.json({ message: "Success" }))
    .catch((err) => {
      return res.status(400).json({ message: err.message });
    });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (
    user &&
    (await bcrypt.compare(String(req.body.password), String(user.password)))
  ) {
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, "secret-jwt-key");
    return res.status(200).json({
      message: "Success",
      jwt_token: jwtToken,
    });
  }
  return res.status(400).json({ message: "Not authorized" });
};

module.exports = {
  registerUser,
  loginUser,
};
