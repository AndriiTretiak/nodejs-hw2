const { Note } = require("./models/Notes.js");

function addUserNotes(req, res, next) {
  const userId = req.user.userId;
  const { text } = req.body;
  try {
    const note = new Note({
      text: text,
      userId: userId,
    });

    note.save().then((saved) => {
      return res.status(200).send({ message: "Success" });
    });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
}

function getUserNotes(req, res, next) {
  try {
    const notes = Note.find(
      { userId: req.user.userId },
      "-__v",
      (err, result) => {
        res.status(200).json({
          offset: req.query.offset || 0,
          limit: req.query.limit || 0,
          count: notes.length,
          notes: result.slice(
            req.query.offset,
            req.query.limit || notes.length
          ),
        });
      }
    );
  } catch (error) {
    return res.status(400).send({
      message: err.message,
    });
  }
}

const getUserNoteById = (req, res, next) => {
  try {
    return Note.find({ userId: req.user.userId }, "-__v").then((result) => {
      res.status(200).json({ note: result });
    });
  } catch (err) {
    return res.status(500).send({
      message: err.message,
    });
  }
};

const updateUserNoteById = (req, res, next) => {
  try {
    const { text } = req.body;
    return Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user._id },
      { text: text }
    ).then((result) => {
      res.status(200).send({ message: "Success" });
    });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
};

const toggleCompletedForUserNoteById = (req, res, next) => {
  try {
    return Note.findByIdAndUpdate(
      { _id: req.params.id, userId: req.user.userId },
      { $set: { completed: true } }
    ).then((result) => {
      res.status(200).json({ message: "Success" });
    });
  } catch (err) {
    return res.status(400).send({
      message: error.message,
    });
  }
};

const deleteUserNoteById = (req, res, next) => {
  try {
    Note.findByIdAndDelete(req.params.id).then((note) => {
      res.status(200).send({ message: "Success", note: note });
    });
  } catch (err) {
    return res.status(400).send({
      message: err.message,
    });
  }
};

module.exports = {
  addUserNotes,
  getUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
  deleteUserNoteById,
};
