const express = require("express");
const { authMiddleware } = require("./middleware/authMiddleware.js");
const router = express.Router();
const {
  getUser,
  deleteUser,
  updateUserPassword,
} = require("./usersService.js");

router.get("/me", authMiddleware, getUser);

router.delete("/me", deleteUser);

router.patch("/me", authMiddleware, updateUserPassword);

module.exports = {
  usersRouter: router,
};
