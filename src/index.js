const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect(
    'mongodb+srv://NodeHWsAT:guess-password-23@clusterat.xqs4gdm.mongodb.net/?retryWrites=true&w=majority',
);

const {notesRouter} = require('./notesRouter.js');
const {authRouter} = require('./authRouter.js');
const {usersRouter} = require('./usersRouter.js');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/notes', notesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}
